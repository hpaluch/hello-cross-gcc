// hello-cross.c - example C program for cross-compiler

#include<stdio.h>
#include<stdlib.h>
#include<string.h> // memset(3)
#include<sys/utsname.h> // uname(2)

int main(int argc, char **argv)
{
	struct utsname buf;

	// automatic variables are not initialized
	// by ANSI C compiler
	memset(&buf,0,sizeof(struct utsname));

	if ( uname(&buf) ){
		perror("uname");
		return EXIT_FAILURE;
	}

	printf("Hello %s machine!\n", buf.machine);
	return EXIT_SUCCESS;
}
