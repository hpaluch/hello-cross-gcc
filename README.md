# hello-cross-gcc

Example CI project using various cross-gcc compilers

# openSUSE 15.3 setup

This also applies for gitlab-runner shell executor.

Install gcc and make:
```bash
sudo zypper in gcc make
```

To support aarch64 (ARM 64-bit) builds you have to install:
```bash
cd
curl -fLO https://releases.linaro.org/components/toolchain/binaries/latest-7/aarch64-linux-gnu/gcc-linaro-7.5.0-2019.12-x86_64_aarch64-linux-gnu.tar.xz
sudo tar xvf gcc-linaro-7.5.0-2019.12-x86_64_aarch64-linux-gnu.tar.xz -C /opt
sudo chown -R root:root /opt/gcc-linaro-7.5.0-2019.12-x86_64_aarch64-linux-gnu/
```
Do NOT use `cross-aarch64-gcc10`! It does NOT include
glibc headers/libraries!

To run 64-bit ARM (aarch64) binary under QEMU _USER_ emulator
you have to install:
```bash
sudo zypper in qemu-linux-user
```
WARNING! It is very different from QEMU _SYSTEM_ emulator (this
one emulates hardware, while _USER_ emulator can run directly
ELF binary)

To validate YAML file install:
```bash
sudo zypper in python3-yamllint
```

# openSUSE 15.3 tips

To have vim with syntax color for C and Makefile, install:
```bash
sudo zypper in vim vim-data
```
