# Makefile for hello-cross.c

CFLAGS += -Wall

all: hello-cross

hello-cross: hello-cross.c

.PHONY: all clean
clean:
	rm -f hello-cross
